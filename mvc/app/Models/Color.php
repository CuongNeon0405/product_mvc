<?php

namespace Mvc\Models;

class Color extends AbstractModel
{
    protected static $table = 'color';
    public $id_color;
    public $name_color;

    public function __construct($item)
    {
        $this->id_color = $item['id_color'];
        $this->name_color = $item['name_color'];
    }
}