<?php

namespace Mvc\Models;

class Product extends AbstractModel
{
    protected static $table = 'product';
    public $id;
    public $title;
    public $price;
 
    public function __construct($item)
    {
        $this->id = $item['id'];
        $this->title = $item['title'];
        $this->price = $item['price'];
    

    }
    public function selectcolor($id){
        $list = [];
        $db = self::getInstance();
        $seachcolor   =  array_values($id);
    
   
        
           $req = $db->query("SELECT DISTINCT product.id  , product.title,  product.price FROM product , productcolor WHERE product.id = productcolor.id_product AND  productcolor.id_color in (".implode(',', $seachcolor) . ')');
           foreach ($req->fetchAll() as $item) {
            $list[] = new static($item);
        }
  
        
              
            return  $list;

       
      
    }
     

    public function  GetProductCat($catid){
      $list =  [];
      $db = self::getInstance();
     $req =  $db->query("SELECT DISTINCT product.id,product.title,product.price FROM product, productcategory, category WHERE (product.id = productcategory.id_product AND productcategory.id_cat = $catid) or (product.id = productcategory.id_product AND productcategory.id_cat = category.id_cat AND category.parenID = $catid)");
       foreach ($req->fetchAll() as $row){
            $list[] = new static($row);
        }
        return $list;

    }
    public function  details($id){
        $list = [];
        $db =  self::getInstance();
        $req = $db->query("SELECT * FROM product , productcategory , category , productcolor , productsize , color ,size where product.id = 63 and (product.id = productcategory.id_product and productcategory.id_cat = category.id_cat ) and( product.id = productcolor.id_product and productcolor.id_color = color.id_color) and (product.id=productsize.id_product and productsize.id_size = size.id)");
        foreach ($req->fetchAll() as $key => $value) {
                        $list[] = new static($value);

        }
        return  $list;
    }
  
}
?>