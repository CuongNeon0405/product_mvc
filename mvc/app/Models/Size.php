<?php

namespace Mvc\Models;

class Size  extends AbstractModel
{
    protected static $table = 'size';
    public $id_size;
    public $name_size;

    public function __construct($item)
    {
        $this->id_size = $item['id'];
        $this->name_size = $item['size'];
        // $this->price = $item['price'];
    }
}
?>