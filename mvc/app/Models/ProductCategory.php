<?php

namespace Mvc\Models;

class ProductCategory extends AbstractModel
{
    protected static $table = 'productcategory';
    public $id;
    public $id_product;
    public $id_cat;

    public function __construct($item)
    {
        $this->id = $item['id'];
        $this->id_product = $item['id_product'];
        $this->id_cat = $item['id_cat'];
    }
  
}
?>