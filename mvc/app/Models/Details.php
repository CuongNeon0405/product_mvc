<?php

namespace Mvc\Models;

class Details extends AbstractModel
{
 
    public $id;
    public $title;
    public $price;
    public $name_cat;
    public $name_color;
    public $size ;
    public function __construct($item)
    {
        $this->id = $item['id'];
        $this->title = $item['title'];
        $this->price = $item['price'];
        $this->name_cat = $item['name_cat'];
        $this->name_color  = $item['name_color'];
        $this->size  = $item['size'];

    }
    
    public function  details($id){
        $list = [];
        $db =  self::getInstance();
        $req = $db->query("SELECT * FROM product , productcategory , category , productcolor , productsize , color ,size where product.id = $id and (product.id = productcategory.id_product and productcategory.id_cat = category.id_cat ) and( product.id = productcolor.id_product and productcolor.id_color = color.id_color) and (product.id=productsize.id_product and productsize.id_size = size.id)");
        foreach ($req->fetchAll() as $key => $value) {
                        $list[] = new static($value);

        }
        return  $list;
    }
  
}
?>