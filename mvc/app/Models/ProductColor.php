<?php

namespace Mvc\Models;

class ProductColor extends AbstractModel
{
    protected static $table = 'productcolor';
    public $id;
    public $id_product;
    public $id_color;

    public function __construct($item)
    {
        $this->id = $item['id'];
        $this->id_product = $item['id_product'];
        $this->id_color = $item['id_color'];
    }
  
}
?>