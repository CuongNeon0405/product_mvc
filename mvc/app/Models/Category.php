<?php

namespace Mvc\Models;
use \PDO;

class Category extends AbstractModel
{
    protected static $table = 'category';
    public $id;
    public $name_cat;
    public $parentID;

    public function __construct($item)
    {
        $this->id = $item['id_cat'];
        $this->name_cat = $item['name_cat'];
        $this->parentID = $item['parenID'];
    }
    public function All_Category() {
       $tmp = self::all();
       $categories = [];

       foreach ($tmp as $item) {
          if ($item->parentID == null) {
            $catChild = [];
            foreach ($tmp as $subItem) {
                if ($subItem->parentID == $item->id) {
                    $catChild[] = $subItem;
                }
            }
            $categories[] = [
                'item' => $item,
                'subItem' => $catChild
            ];
          }
       }
       return $categories;
    }
    public function ByParrentId($id){
        
                   $parent_id =  $id ;
        $db = self::getInstance()
        ;

           $obj_select = $db->prepare("SELECT * FROM ".static::$table." WHERE `parenID` = $parent_id");
         
         

            $obj_select->execute();

            $categorieParrents = $obj_select->fetchAll(PDO::FETCH_ASSOC);
                    
           return $categorieParrents;
            
        
    }


    //  public  function ischiId(){
    //     $check = false ;
    //     if ($this->prampe > 0) {
    //         $check = true;
    //     } 
    //     return $check;
            
        
    // }    
    //  public function fetchChild()
    // {
    //             $db = self::getInstance();
    //  // var_dump($db->fetch(static::$table, ' parenID = ' . $this->data['id_cat']));
    //     return $db->fetchAll(static::$table, ' parenID = ' . $this->data['id_cat']);
    // }

    // public function isParent()
    // {
    //     $check = false;
    //     if(count($this->fetchChild($this->data['id_cat'])) > 0) {
    //         $check = true;
    //     }
    //     return $check;
    // }

}

?>