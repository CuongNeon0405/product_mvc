<?php

namespace Mvc\Controllers;

class ProductController extends BaseController
{
    public $folder = 'product';

    public function view($id = null)
    {
        $product = \Mvc\Models\Product::getById($id);

        $this->render('view', [
            'product' => $product
        ]);
    }

    public function listing()
    {
        $products = \Mvc\Models\Product::all();
        $colors = \Mvc\Models\Color::all();
       
        $category  = \Mvc\Models\Category::All_Category();


        $this->render('list', [
            'title' => 'Products',
            'products' => $products,
            'colors'=>$colors,
            'category'=>$category
        ]);
    }
    public function AddProduct(){
        $colors = \Mvc\Models\Color::all();
        $sizes = \Mvc\Models\Size::all();
        $category  = \Mvc\Models\Category::All_Category();

             $this->render('add', [
            'title' => 'Add Products',
            'colors' => $colors,
            'sizes' => $sizes,
            'category'=>$category

        ]);
    }
    public function AddPostproduct(){

            $title =  $_POST['title'];
            $price = $_POST['price'];
            $colors = $_POST['colors'];
            $sizes =  $_POST['sizes'];
            $category = $_POST['categorys'];

            $item = ['title' => $title, 'price'=>$price];
            $id = \Mvc\Models\Product::insert($item);
            for($i=0;$i<count($colors);$i++){
                $itemcolor =  ['id_product'=>$id , 'id_color'=>$colors[$i]];
                $insertcolor = \Mvc\Models\ProductColor::insert($itemcolor);
            }
            for($i=0;$i<count($sizes);$i++){
                $itemsize =  ['id_product'=>$id , 'id_size'=>$sizes[$i]];
                $insertcolor = \Mvc\Models\ProductSize::insert($itemsize);
            }
            $itemcat = ['id_product'=>$id , 'id_cat'=>$category];
            $insertcate =  \Mvc\Models\ProductCategory::insert($itemcat);

          

    }
    public function  seachColor(){
        if (!isset($_POST['color'])) {
                    $products = \Mvc\Models\Product::all();
  
        foreach ($products as $id) {
           
            echo '<div class="col-md-3"><img src="./app/img/product.jpeg"><br>
            <div class="col-md-9">
            <h4><a href="?scope=product&action=details&id= '. $id->id.'"> '.   $id->title .'</a></h4>
              <p class="price">  Price $'. $id->price.'</p></div>';
             echo "</div>";
             echo "</div>";
        }
            return;
        }
        $id  = $_POST['color'];
        $ColorProduct = \Mvc\Models\Product::selectcolor($id);
         
        foreach ($ColorProduct as $id) {
         
            echo '<div class="col-md-3"><img src="./app/img/product.jpeg"><br>
            <div class="col-md-9">
            <h4><a href="?scope=product&action=details&id= '. $id->id.'"> '.   $id->title .'</a></h4>
            
              <p class="price">  Price $'. $id->price.'</p></div>';
             echo "</div>";
             echo "</div>";
        }
        
    }
  
    public function GetCatList($id = null){
        $colors = \Mvc\Models\Color::all();        
        $category  = \Mvc\Models\Category::All_Category();
         $catId = $id ;
          $products = [];
        if($catId != null) {
            $products = \Mvc\Models\Product::GetProductCat($catId);
        } else {
            $products = \Mvc\Models\Product::all();
        }

        $this->render('list', [
            'title' => 'Products',
            'products' => $products,
            'colors'=>$colors,
            'category'=>$category
        ]);
    }
    public function details($id){
        $product  =  \Mvc\Models\Details::details($id);
         echo "</pre>";
         $this->render('details', [
            'title' => 'Products',
            'product' => $product,
          
        ]);

    }
}

