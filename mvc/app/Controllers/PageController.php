<?php

namespace Mvc\Controllers;

class PageController extends BaseController
{
    public $folder = 'page';

    public function home()
    {
        $this->render('home',[
            'title' => 'Hello  smartosc',
        ]);
    }

    public function contact()
    {
        $product = \Mvc\Models\Product::getById(3);
        $this->render('contact',
            [
                'email' => 'cuong@smartosc.com',
                'skype' => 'lucuong837@gmail.com',
                'product' => $product
            ]);
    }
}