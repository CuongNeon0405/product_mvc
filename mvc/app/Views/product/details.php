<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.0/css/bootstrap.min.css" integrity="sha384-SI27wrMjH3ZZ89r4o+fGIJtnzkAnFs3E4qz9DIYioCQ5l9Rd/7UAa8DHcaL8jkWt" crossorigin="anonymous">
</head>

<body>

<div class="container">
   <h3 style="text-align: center;">Product table by category and color, size</h3>
<table class="table">
	<tr>
		<th>Product Name</th>
		<th>Category</th>
		<th>Size</th>
		<th>Color</th>
	</tr>
	 <?php foreach ($product as $product) : ?>
              <tr>
           <td><?php echo $product->title?></td>
    
           <td><?php echo $product->name_cat?></td>
           <td><?php echo $product->size?></td>
           <td><?php echo $product->name_color?></td>
         </tr>
      
        <?php endforeach; ?>
   
</table>


    
</div>
</body>

</html>