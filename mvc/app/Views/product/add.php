<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.0/css/bootstrap.min.css" integrity="sha384-SI27wrMjH3ZZ89r4o+fGIJtnzkAnFs3E4qz9DIYioCQ5l9Rd/7UAa8DHcaL8jkWt" crossorigin="anonymous">
           <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
           <script type="text/javascript">
               $(document).ready(function(){
                $("button").click(function(){
                  var  title =  $("[name=title]");
                  var price = $("[name=price]");
                   var colors = $("[name=color]:checked").map(function(_,el){
                return $(el).val();
            }).get();
            var sizes = $("[name=size]:checked").map(function(_,el){
                                return $(el).val();
            }).get();
                  var categorys = $("[name=category]");
                if (title.val() == "") {
          document.getElementById('title').innerHTML = "<p class='text-danger'>Please enter the product title *</p>"
                        return;
                    } else {
                        document.getElementById('title').innerHTML = "";
                    }

                    if(price.val() == "") {
                        document.getElementById('price').innerHTML = "<p class='text-danger'>Please enter the price *</p>"
                        return;
                    } else {
                        document.getElementById('price').innerHTML = "";
                    }
                     if(colors.length == 0){
                        document.getElementById('color').innerHTML = "<p class='text-danger'>Please select a color *</p>"
                        return;
                    } else {
                        document.getElementById('color').innerHTML = "";
                    }
                    if(sizes.length == 0){
                        document.getElementById('size').innerHTML = "<p class='text-danger'>Please select a size  *</p>"
                        return;
                    } else {
                        document.getElementById('size').innerHTML = "";
                    }
                      if(categorys.val() == ''){
                        document.getElementById('category').innerHTML = "<p class='text-danger'>Please select a category *</p>"
                        return;
                    } else {
                        document.getElementById('category').innerHTML = "";
                    }
                    data =  {
                        "title": title.val(),
                         "price": price.val(),
                         "colors": colors,
                        "sizes": sizes,
                        "categorys":categorys.val(),
                    }
                  console.log(data);

                  
                      $.post('?scope=product&action=AddPostproduct',data,function(data,status){
                        alert( 'create '+status)
                    });

                });
               });
           </script>
</head>

<body>


<div class="container">
    <h1><?= $title ?></h1>
  
    <div class="row">
        <div class="col-md-3">
            <label>Title </label>
        </div>
        <div class="col-md-6">
            <input type="text"  class="form-control" name="title">
            <br>
    <div id="title"></div>
        </div>
    </div>

    <hr>

     <div class="row">
        <div class="col-md-3">
            <label>Price </label>
        </div>
        <br>
        <div class="col-md-6">
            <input type="number"  class="form-control" name="price">
            <br>
            <div id="price"></div>
        </div>
       
    </div>
 
  
 
    <hr>

     <div class="row">
        <div class="col-md-3">
            <label>Color </label>
        </div>
        <div class="col-md-6">
              <?php
              
                foreach ($colors as $value)
                {

                 ?>
                 <div class="form-check">
              <input class="form-check-input" type="checkbox" name="color" value="<?php    echo  $value->id_color;?>" id="defaultCheck1">
              <label class="form-check-label" for="defaultCheck1">
            <?php echo  $value->name_color;?>
              </label>
                </div>
                 <?php

                }
                ?>
                <br>
                <div id="color"></div>
                  </div>
    
    </div>

    <hr>
     <div class="row">
        <div class="col-md-3">
            <label>Size </label>
        </div>
        <br>
        <div class="col-md-6">
              <?php
              
                foreach ($sizes as $value)
                {
                 ?>
                 <div class="form-check">
              <input class="form-check-input" type="checkbox" name="size" value="<?php echo $value->id_size;?>" id="defaultCheck1">
              <label class="form-check-label" for="defaultCheck1">
            <?php echo  $value->name_size;?>
              </label>
                </div>
                 <?php

                }
                ?>
                  </div>
                          <div id="size"></div>

    </div>

        <hr>

      <div class="row">
        <div class="col-md-3">
            <label>Category </label>
        </div>
        <div class="col-md-6">
             <select name="category" size="10">
                <?php 
                            foreach ($category  as $item)
                    {
                      
                   ?>
                    <optgroup label="<?php echo $item['item']->name_cat; ?>"> 
     
                        <?php 
                        if (count($item['subItem']) > 0) {
                        foreach ($item['subItem'] as $subItem) {
                   
                            echo "<option value='$subItem->id' name='cat'>". $subItem->name_cat ."</option>";
                        }
                    }
                        ?>
                    </optgroup>
 
                 <?php       
                        }
                ?>
                 
             </select>
                  </div>
                  <div id="category"></div>

    </div>
    <br>
    <div class="row" style="margin-bottom: 20px;">
                    <div class="col-md-3"></div>
                    <div class="col-md-9">
                        <button class="btn btn-primary" type="button">Save</button>
                    </div>
                </div>

</div>

</div>

</body>

</html>