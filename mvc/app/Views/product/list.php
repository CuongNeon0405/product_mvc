<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.0/css/bootstrap.min.css" integrity="sha384-SI27wrMjH3ZZ89r4o+fGIJtnzkAnFs3E4qz9DIYioCQ5l9Rd/7UAa8DHcaL8jkWt" crossorigin="anonymous">
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
           <script type="text/javascript">
            function check(){
              let color = [];
              elements = $("input[name='color']:checked");
              for (i = 0; i < elements.length; i++) {
                  color.push(elements[i].value);
              }
              data = {
                'color' : color
              }
              console.log(color);
                 $.post('?scope=product&action=seachColor',data,function(data,status){
                        $('#pr').html(data)
                    });

            }
            
          
           </script>
</head>

<body>


<div class="container">
   <div class="row">
    <div class="col-md-3">
           <div class="row">
        
        <div class="col-md-6" id="list">
          Color

              <?php
              
                foreach ($colors as $key => $value)
                {

                 ?>
                 <div class="form-check">
              <input class="form-check-input" onchange="check()" type="checkbox" name="color" value="<?php    echo  $value->id_color;?>" id="defaultCheck<?=$key?>">
              <label class="form-check-label" for="defaultCheck<?=$key?>">
            <?php echo  $value->name_color;?>
              </label>
                </div>
                 <?php

                }
                ?>
                
                  </div>
  
    </div>
    <br>
  <div >
  <div ><b><a href="?scope=product&action=listing">All</a></b></div> 
  <?php foreach ($category as $item ): 
       
  ?>
                            <div ><b>
                              <a href="?scope=product&action=GetCatList&id=<?=$item['item']->id?>"><?php echo $item['item']->name_cat; ?></a></b></div>
                              <?php 
                        if (count($item['subItem']) > 0) {
                        foreach ($item['subItem'] as $subItem) {
                   
                            echo "<div ><a href='?scope=product&action=GetCatList&id= $subItem->id' style='margin: 12px;'>". $subItem->name_cat ."</a></div>";
                        }
                    }
                        ?>
                            
                            <?php endforeach; ?>
  </div>
  
    </div>
    <div class="col-md-9">
        <div class="row" id="pr">
        <?php foreach ($products as $product) : ?>
          <div class="col-md-3"><img src="./app/img/product.jpeg">
            <br>
            <div class="col-md-9">
            <h4><a href="?scope=product&action=details&id=<?php echo $product->id; ?>"> <?php echo $product->title;?></a> </h4>
          
             <p class="price">  Price $<?php echo $product->price;?></p>
         </div>
       </div>
        <?php endforeach; ?>
    </div>
</div>
    
</div>
</div>

</body>

</html>