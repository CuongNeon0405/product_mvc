-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 09, 2019 at 08:26 PM
-- Server version: 5.7.28-0ubuntu0.18.04.4
-- PHP Version: 7.2.24-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `demo_mvc`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id_cat` int(11) NOT NULL,
  `name_cat` varchar(40) NOT NULL,
  `parenID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id_cat`, `name_cat`, `parenID`) VALUES
(13, 'LapTop', NULL),
(14, 'IPad', NULL),
(15, 'Accessories', NULL),
(16, 'phone', NULL),
(17, 'DELL', 13),
(18, 'Assus', 13),
(19, 'HP', 13),
(20, 'ACER', 13),
(21, 'HEADPHONE', 15),
(22, 'KEYBOARD', 15),
(23, 'samsung', 14),
(24, 'lenovo', 14),
(25, 'Huawei', 14),
(26, 'nokia', 16),
(27, 'HTC', 16),
(28, 'samsung', 16);

-- --------------------------------------------------------

--
-- Table structure for table `color`
--

CREATE TABLE `color` (
  `id_color` int(11) NOT NULL,
  `name_color` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `color`
--

INSERT INTO `color` (`id_color`, `name_color`) VALUES
(1, 'red'),
(2, 'yellow'),
(3, 'blue'),
(4, 'black');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `title` varchar(225) NOT NULL,
  `price` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `title`, `price`) VALUES
(62, 'Dell-99', '12000'),
(63, 'Dell-HD', '15000'),
(64, 'Dell-HD5', '15000'),
(65, 'Assus-HD5', '10000'),
(66, 'HP-HD5', '30000'),
(67, 'HP-HD10', '60000'),
(68, 'ACER-LOL', '70000'),
(69, 'BLUETOOTH HEADPHONE', '70084'),
(70, 'KEYBOARD', '70099'),
(71, 'samsung G4 ', '70120'),
(72, 'samsung G5', '60120'),
(73, 'lenovo G5', '100120'),
(74, 'lenovo G6', '10012'),
(75, 'Huawei G6', '10012'),
(76, 'Huawei G7', '10030'),
(77, 'nokia 170', '10030'),
(78, 'nokia plus', '40030'),
(79, 'HTC plus', '40030'),
(80, 'samsung plus', '40030');

-- --------------------------------------------------------

--
-- Table structure for table `productcategory`
--

CREATE TABLE `productcategory` (
  `id` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `id_cat` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `productcategory`
--

INSERT INTO `productcategory` (`id`, `id_product`, `id_cat`) VALUES
(1, 63, 17),
(2, 64, 17),
(3, 65, 18),
(4, 66, 19),
(5, 67, 19),
(6, 68, 20),
(7, 69, 21),
(8, 70, 22),
(9, 71, 23),
(10, 72, 23),
(11, 73, 24),
(12, 74, 24),
(13, 75, 25),
(14, 76, 25),
(15, 77, 26),
(16, 78, 26),
(17, 79, 27),
(18, 80, 28);

-- --------------------------------------------------------

--
-- Table structure for table `productcolor`
--

CREATE TABLE `productcolor` (
  `id` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `id_color` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `productcolor`
--

INSERT INTO `productcolor` (`id`, `id_product`, `id_color`) VALUES
(36, 62, 1),
(37, 62, 2),
(38, 63, 1),
(39, 63, 4),
(40, 64, 4),
(41, 65, 3),
(42, 65, 4),
(43, 66, 1),
(44, 66, 3),
(45, 66, 4),
(46, 67, 1),
(47, 67, 2),
(48, 67, 3),
(49, 67, 4),
(50, 68, 1),
(51, 68, 2),
(52, 68, 3),
(53, 68, 4),
(54, 69, 1),
(55, 69, 3),
(56, 69, 4),
(57, 70, 1),
(58, 70, 3),
(59, 71, 1),
(60, 71, 2),
(61, 71, 3),
(62, 72, 1),
(63, 72, 3),
(64, 73, 1),
(65, 73, 3),
(66, 73, 4),
(67, 74, 1),
(68, 74, 2),
(69, 74, 3),
(70, 74, 4),
(71, 75, 1),
(72, 75, 2),
(73, 75, 3),
(74, 76, 1),
(75, 76, 2),
(76, 76, 3),
(77, 76, 4),
(78, 77, 1),
(79, 77, 2),
(80, 77, 3),
(81, 77, 4),
(82, 78, 1),
(83, 78, 4),
(84, 79, 1),
(85, 79, 2),
(86, 79, 4),
(87, 80, 1),
(88, 80, 4);

-- --------------------------------------------------------

--
-- Table structure for table `productsize`
--

CREATE TABLE `productsize` (
  `id` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `id_size` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `productsize`
--

INSERT INTO `productsize` (`id`, `id_product`, `id_size`) VALUES
(35, 62, 2),
(36, 62, 3),
(37, 63, 2),
(38, 63, 3),
(39, 63, 4),
(40, 64, 1),
(41, 64, 2),
(42, 64, 3),
(43, 64, 4),
(44, 65, 1),
(45, 65, 3),
(46, 65, 4),
(47, 66, 3),
(48, 66, 4),
(49, 67, 1),
(50, 67, 3),
(51, 67, 4),
(52, 68, 1),
(53, 68, 2),
(54, 68, 3),
(55, 68, 4),
(56, 69, 1),
(57, 69, 2),
(58, 69, 3),
(59, 69, 4),
(60, 70, 2),
(61, 70, 3),
(62, 70, 4),
(63, 71, 3),
(64, 71, 4),
(65, 72, 3),
(66, 72, 4),
(67, 73, 2),
(68, 73, 3),
(69, 73, 4),
(70, 74, 2),
(71, 74, 3),
(72, 74, 4),
(73, 75, 3),
(74, 75, 4),
(75, 76, 3),
(76, 76, 4),
(77, 77, 1),
(78, 77, 3),
(79, 77, 4),
(80, 78, 1),
(81, 78, 4),
(82, 79, 1),
(83, 79, 3),
(84, 79, 4),
(85, 80, 1),
(86, 80, 3),
(87, 80, 4);

-- --------------------------------------------------------

--
-- Table structure for table `size`
--

CREATE TABLE `size` (
  `id` int(11) NOT NULL,
  `size` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `size`
--

INSERT INTO `size` (`id`, `size`) VALUES
(1, 'M'),
(2, 'S'),
(3, 'Y'),
(4, 'L');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id_cat`),
  ADD KEY `parenID` (`parenID`);

--
-- Indexes for table `color`
--
ALTER TABLE `color`
  ADD PRIMARY KEY (`id_color`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `productcategory`
--
ALTER TABLE `productcategory`
  ADD PRIMARY KEY (`id`),
  ADD KEY `productcategory_ibfk_1` (`id_cat`),
  ADD KEY `id_product` (`id_product`);

--
-- Indexes for table `productcolor`
--
ALTER TABLE `productcolor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_product` (`id_product`),
  ADD KEY `id_color` (`id_color`);

--
-- Indexes for table `productsize`
--
ALTER TABLE `productsize`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_product` (`id_product`),
  ADD KEY `id_size` (`id_size`);

--
-- Indexes for table `size`
--
ALTER TABLE `size`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id_cat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `color`
--
ALTER TABLE `color`
  MODIFY `id_color` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;
--
-- AUTO_INCREMENT for table `productcategory`
--
ALTER TABLE `productcategory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `productcolor`
--
ALTER TABLE `productcolor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;
--
-- AUTO_INCREMENT for table `productsize`
--
ALTER TABLE `productsize`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;
--
-- AUTO_INCREMENT for table `size`
--
ALTER TABLE `size`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `category`
--
ALTER TABLE `category`
  ADD CONSTRAINT `category_ibfk_1` FOREIGN KEY (`parenID`) REFERENCES `category` (`id_cat`);

--
-- Constraints for table `productcategory`
--
ALTER TABLE `productcategory`
  ADD CONSTRAINT `productcategory_ibfk_1` FOREIGN KEY (`id_cat`) REFERENCES `category` (`id_cat`),
  ADD CONSTRAINT `productcategory_ibfk_2` FOREIGN KEY (`id_product`) REFERENCES `product` (`id`);

--
-- Constraints for table `productcolor`
--
ALTER TABLE `productcolor`
  ADD CONSTRAINT `productcolor_ibfk_1` FOREIGN KEY (`id_product`) REFERENCES `product` (`id`),
  ADD CONSTRAINT `productcolor_ibfk_2` FOREIGN KEY (`id_color`) REFERENCES `color` (`id_color`);

--
-- Constraints for table `productsize`
--
ALTER TABLE `productsize`
  ADD CONSTRAINT `productsize_ibfk_1` FOREIGN KEY (`id_product`) REFERENCES `product` (`id`),
  ADD CONSTRAINT `productsize_ibfk_2` FOREIGN KEY (`id_size`) REFERENCES `size` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
