<?php

$scope = isset($_REQUEST['scope']) ? $scope = $_REQUEST['scope'] : null;
$action = isset($_REQUEST['action']) ?$_REQUEST['action'] : null;
$id = isset($_REQUEST['id'])? $_REQUEST['id'] : null;

$router = [
    'product' => ['view', 'listing','AddProduct' , 'AddPostproduct','seachColor','GetCatList','details'],
    'page' => ['home', 'contact']
];

if (!array_key_exists($scope,$router) || !in_array($action, $router[$scope])) {
    $scope = 'page';
    $action = 'home';
}

$class = '\\Mvc\\Controllers\\' .ucwords($scope) . 'Controller';
$controller = new $class();
$controller->$action($id);
